<?php
/**
* Plugin Name: Imobiliare API Import
* Plugin URI: http://costacheadrian.com/imoAPI
* Description: Plugin de importare continut din www.imobiliare.ro.
* Version: 1.0
* Author: Adrian Costache
* Author URI: Author's website
* License: A "Slug" license name e.g. GPL12
*/
/******************************************************************************************************
CREATE XML FILES FOR IMPORT CONTENT
/*****************************************************************************************************/
set_time_limit(0);
define( 'MY_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'API_ENDPOINT', 'http://apiws.imobiliare.ro/index.php?wsdl' );

add_action( 'wp_enqueue_scripts', 'enqueue_my_scripts' );
add_action( 'admin_menu', 'Create_import_content_Admin_menu' );
add_action( 'admin_init', 'imo_settings_init' );
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );
// add_action("init", "populate_xml_files_importer");
add_action( 'add_meta_boxes', 'detalii_anunt_add_meta_box' );
// add_action( 'init','terenuri_category' );
add_action( 'init','apartamente_category' );
add_action( 'init','spatii_category' );
add_action( 'init','casevile_category' );
add_action('wp_ajax_populate_xml_files_importer','populate_xml_files_importer');
add_action('wp_ajax_delete_anunturi','delete_anunturi');

function delete_anunturi(){

    $sql = "delete p,pm from wp_posts p join wp_postmeta pm on pm.post_id = p.id where p.post_type = 'property'";
    $results = $wpdb->get_results($sql);
    die();

}

// add_action( 'init','_APIconnectancreateXMLapartamente' );

function _APIconnectancreateXMLapartamente() {
        // ------------------------------------------------------------------------------------------------------
        // connect to api

global $options;
$options = get_option( 'imo_settings' );
$ip = $options["ipseting"];
$userkey = $options["userkey"];
$userpass = $options["userpassword"];
// $requestnr = $options["requestnr"];


if ($userkey !=="" && $userpass !=="") {

     // Apeluri API

        $s = new SoapClient( API_ENDPOINT );
        $s->timeout = 3600000;
        $s->response_timeout = 3600000;
        $params = array(

            'login' => array(

                'id'        => $userpass,
                'parola'    => $userkey,
                'ip'        =>  $ip,
                'env'      => 'dev',
            )
        );
        try {
        $result = $s->__soapCall( 'login',                        // method name
                        $params                        // input parameters
        );
        } catch( Exception $e ) {
        die( 'Eroare: ' . $e->getMessage() );
        }
        $aExtra = explode( '#', $result->extra );
        $sSessionId = trim( @$aExtra[ 1 ] );

        /**
        * SETARE VERSIUNE
        *
        */
        $params = array(
        'setare_versiune' => array(
        'sid'    => $sSessionId,
        'versiune'  => 3,
        )
        );

        try {
        $result = $s->__soapCall( 'setare_versiune',   // method name
                    $params                        // input parameters
        );
        } catch( Exception $e ) {
        die( 'Eroare: ' . $e->getMessage() );
        }

        $params = array(

        'update_all ' => array(
        'sid'    => $sSessionId,

        )
        );

        try {
        $result = $s->__soapCall( 'update_all',                        // method name
                    $params                        // input parameters
        );
        } catch( Exception $e ) {
        die( 'Eroare: ' . $e->getMessage() );
        }

        // afisam ce am primit
        // cautam 20 id-uri de oferte ca sa le preluam
        preg_match_all('/(X[a-zA-Z0-9]{8})/', $result->extra, $result, PREG_PATTERN_ORDER);
        $aOferte = array_slice( $result[0], 2, 5000);

        /**
        * OBTINE OFERTE - avand unul sau mai multe ID-uri, obtinem si ofertele respective
        */
        $params = array(
        'obtine_oferte_all' => array(
        'sid'    => $sSessionId,
        'oferte'     => implode( ',', $aOferte),
        )
        );

        try {
        $result = $s->__soapCall( 'obtine_oferte_all',                        // method name
                        $params                        // input parameters
        );
        } catch( Exception $e ) {
        die( 'Eroare: ' . $e->getMessage() );
        }
        $doc = new DomDocument( );

         $upload_dir = wp_upload_dir();
         $save_path = MY_PLUGIN_PATH .'/xml/oferte.xml';

        $doc->LoadXml($result->extra );


        $doc->save($save_path);

}




    }

// add_action( 'init','_APIconnectancreateXMLAgenti' );
function _APIconnectancreateXMLAgenti() {
        // ------------------------------------------------------------------------------------------------------
        // connect to api

global $options;
$options = get_option( 'imo_settings' );
$ip = $options["ipseting"];
$userkey = $options["userkey"];
$userpass = $options["userpassword"];
// $requestnr = $options["requestnr"];

// define( 'API_ID', 'X5SV' );
// define( 'API_KEY', 'w3J3fWmnEbNgT3Kq7yxB' );

if ($userkey !=="" && $userpass !=="") {
   $s = new SoapClient( API_ENDPOINT );
        $s->timeout = 3600000;
        $s->response_timeout = 3600000;
        $params = array(

            'login' => array(

                'id'        => $userpass,
                'parola'    => $userkey,
                'ip'        =>  $ip,
                'env'      => 'dev',
            )
        );
        try {
        $result = $s->__soapCall( 'login',                        // method name
                        $params                        // input parameters
        );
        } catch( Exception $e ) {
        die( 'Eroare: ' . $e->getMessage() );
        }
        $aExtra = explode( '#', $result->extra );
        @$sSessionId = trim( $aExtra[ 1 ] );

        /**
        * SETARE VERSIUNE
        *
        */
        $params = array(
        'setare_versiune' => array(
        'sid'    => $sSessionId,
        'versiune'  => 3,
        )
        );

        try {
        $result = $s->__soapCall( 'setare_versiune',   // method name
                    $params                        // input parameters
        );
        } catch( Exception $e ) {
        die( 'Eroare: ' . $e->getMessage() );
        }

  preg_match_all('/(X[a-zA-Z0-9]{8})/', $result->extra, $result, PREG_PATTERN_ORDER);
        $aOferte = array_slice( $result[0], 2, 5000);
        $params = array(
        'import_lista_agenti' => array(
        'sid'    => $sSessionId,
        'oferte'     => implode( ',', $aOferte),
        )
        );

        try {
        $result = $s->__soapCall( 'import_lista_agenti',                        // method name
                        $params                        // input parameters
        );
        } catch( Exception $e ) {
        die( 'Eroare: ' . $e->getMessage() );
        }
        $doc = new DomDocument( );

         $upload_dir = wp_upload_dir();
         $save_path = MY_PLUGIN_PATH .'/xml/agenti.xml';

        $doc->LoadXml("<agenti>".$result->extra."</agenti>" );


        $doc->save($save_path);



}


        // Apeluri API

}


function enqueue_my_scripts() {

    wp_enqueue_script( 'jquery' );
    // wp_enqueue_script('fancybox', plugin_dir_url(__FILE__) . 'gallery/fancybox/jquery.fancybox.js', array('jquery'));


    // wp_enqueue_style('gallerystyle', plugin_dir_url(__FILE__) .  'gallery/fancybox/jquery.fancybox.css');
}

function load_custom_wp_admin_style() {
    wp_enqueue_style( 'custom_wp_admin_css', plugins_url('/admin/css/bootstrap.css', __FILE__) );
    wp_enqueue_style( 'custom_wp_admin_settings_css', plugins_url('/admin/css/RubikImportImoAPi.css', __FILE__) );
    wp_enqueue_style( 'icons', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
    wp_enqueue_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );

    // $scripts_from_wp = array(

    //     'jquery',
    //     'jquery-ui-core',
    //     'jquery-ui-core',
    //     'jquery-ui-widget' ,
    //     'jquery-ui-mouse',
    //     'jquery-ui-accordion',
    //     'jquery-ui-autocomplete',
    //     'jquery-ui-slider',
    //     'jquery-ui-tabs',
    //     'jquery-ui-sortable',
    //     'jquery-ui-draggable',
    //     'jquery-ui-droppable',
    //     'jquery-ui-datepicker',
    //     'jquery-ui-resize',
    //     'jquery-ui-dialog',
    //     'jquery-ui-button',
    // );

    // foreach ($scripts_from_wp as $zet_script) {
    //     $output = wp_enqueue_script($zet_script);
    // }
//
    wp_enqueue_script('jquery');

    wp_enqueue_style( 'jquery_ui', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css' );
    wp_enqueue_script('js-ui', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', array('jquery'));
    wp_enqueue_script('js-loading', plugin_dir_url(__FILE__) . '/js/jquery.ajax-progress.js', array('jquery'));
    wp_enqueue_script('app', plugin_dir_url(__FILE__) . '/js/app.js', array('jquery'));


    wp_localize_script(
    'zet', 'my_ajax_object',
    array(
    'ajax_url' => admin_url( 'admin-ajax.php' )
    )
    );

}

function ajaxurlScript(){
    $script="<script name='arrx'>var ajaxurldel = ".admin_url( 'admin-ajax.php' ).";</script>";
    echo  $script;
}

add_action('admin_header','ajaxurlScript');


/******************************************************************************************************
LOAD NECCESARY SCRIPTS
/*****************************************************************************************************/
function Create_import_content_Admin_menu(  ) {
    global $my_admin_page;
    $my_admin_page = add_menu_page( 'Imobiliare Import', 'Imobiliare Import', 'manage_options', 'imoapi', 'RubikImportImoAPi_options_page' );
    add_action('load-'.$my_admin_page, 'my_admin_add_importer_tab');
}
/******************************************************************************************************
LOAD NECCESARY SCRIPTS
/*****************************************************************************************************/
function imo_settings_init(  ) {

    register_setting( 'pluginPage', 'imo_settings' );
    add_settings_section(
        'imo_pluginPage_section',
        __( 'Setari', 'domain' ),
        'imo_settings_section_callback',
        'pluginPage'
    );
    add_settings_section(
        'imo_pluginPage_section2',
        __( 'Design', 'domain' ),
        'imo_settings_section_callback2',
        'pluginPage'
    );
    add_settings_field(
        'ipseting',
        __( 'Setati IP-ul de pe care se fac request-urile', 'domain' ),
        'ipseting',
        'pluginPage',
        'imo_pluginPage_section'
    );
    add_settings_field(
        'userpassword',
        __( 'Setati user password', 'domain' ),
        'userpassword',
        'pluginPage',
        'imo_pluginPage_section'
    );
    add_settings_field(
        'userkey',
        __( 'Setati user Key', 'domain' ),
        'userkey',
        'pluginPage',
        'imo_pluginPage_section'
    );
}
/******************************************************************************************************
IP SETTINGS
/*****************************************************************************************************/
function ipseting(  ) {
    $options = get_option( 'imo_settings' );
    ?>
    <div class="form-group">
        <input type='text'   class="form-control"  name='imo_settings[ipseting]' value='<?php echo $options['ipseting']; ?>'>
        <label style="background:#000; height:40px; line-height:40px; color:#fff; padding-left:10px;"> IP-ul tau este : <?php echo file_get_contents("http://apiws.imobiliare.ro/ip.php"); ?> </label>
    </div>
    <?php

}
/******************************************************************************************************
PASSWORD SETTINGS
/*****************************************************************************************************/
function userpassword(  ) {

    $options = get_option( 'imo_settings' );
    ?>
    <div class="form-group">

        <input type='text' class="form-control" name='imo_settings[userpassword]' value='<?php echo $options['userpassword']; ?>'>

    </div>
    <?php
}

/******************************************************************************************************
USER KEY SETTINGS
/*****************************************************************************************************/
function userkey(  ) {
    $options = get_option( 'imo_settings' );
    ?>
    <div class="form-group">
    <input type='text'  class="form-control"   name='imo_settings[userkey]' value='<?php echo $options['userkey']; ?>'>
    </div>
    <?php
}

add_action("init","sterge_toate_anunturile");

function sterge_toate_anunturile(){

    if (isset($_POST['sterge_toate_anunturile'])) {

        global $wpdb;

        $query = "DELETE p,pm FROM wp_posts p JOIN wp_postmeta pm ON pm.post_id = p.id WHERE p.post_type = 'property'";
        $wpdb->query($query);
    }

}


/******************************************************************************************************
INFO SETTINGS
/*****************************************************************************************************/

function imo_settings_section_callback(  ) {
    // echo __( 'This section description', 'domain' );
}
function imo_settings_section_callback2(  ) {
    // echo __( 'This section description', 'domain' );
}
function RubikImportImoAPi_options_page(  ) {
    ?>
    <div class="app-wrapp ">
        <h2 class="imo-title">API de importare anunturi de pe imobiliare.ro</h2>
        <div class="imo-panel">

        <form action="#" method="post" >
            <input type="submit" name="sterge_toate_anunturile" class="btn btn-danger" value="Sterge toate anunturile">
        </form>

        <div id="process"><img src="<?php echo plugin_dir_url( __FILE__ ) . 'img/loading.gif'; ?>" alt=""> Va rugam astesptati. Este posibil ca importul anunturiler sa dureze cateva minute.
        </div>

        <div id="succes"><span class="fa fa-check"></span> Anunturile au fost importate cu succes !</div>
            <div class="row app-setting-wrapp">
                <form action='options.php' method='post'>
                <?php
                settings_fields( 'pluginPage' );
                do_settings_sections( 'pluginPage' );
                submit_button();
                ?>
                </form>
            </div>
        </div>
    </div>

    <?php
}

/******************************************************************************************************
IMPORT CONTENT BUTTON
/*****************************************************************************************************/
function my_admin_add_importer_tab () {
    global $my_admin_page;
    $screen = get_current_screen();
    /*
     * Check if current screen is My Admin Page
     * Don't add help tab if it's not
     */
    if ( $screen->id != $my_admin_page ){
        return;
    }
    else{
        add_action( "admin_notices", function() {
            echo "<div class='updated'>";
            echo "<p>";
            echo "Pentru a importa categoriile apasati Importa Categorii ";
            echo "Pentru a importa/reactualiza anunturi  apasati butonul import ";
            ?>
            <form action="#" method="get">
                <p>

                <input type="submit" id="import-anunturi" name="import-anunturi" class="button button-primary" value="Importare Anunturi ">
                <input type="submit" name="import-taxonomyes" class="button button-primary" value="Importa Categorii ">

                </p>
            </form>
            <?php
            echo "</p>";
            echo "</div>";
        });
    }
}

function detalii_anunt_get_meta( $value ) {
    global $post;

    $field = get_post_meta( $post->ID, $value, true );
    if ( ! empty( $field ) ) {
        return is_array( $field ) ? stripslashes_deep( $field ) : stripslashes( wp_kses_decode_entities( $field ) );
    } else {
        return false;
    }
}




/******************************************************************************************************
CREATE APARTAMENTE CATEGORY
/*****************************************************************************************************/
function apartamente_category() {
    wp_insert_term(
        'Apartamente',
        'category',
        array(
        'description' => 'In aceasta categorie se vor importa toate ofertele de tip apartament',
        'slug'        => 'apartamente'
        )
    );
}
/******************************************************************************************************
CREATE CASE VILE CATEGORY
/*****************************************************************************************************/
function casevile_category() {
    wp_insert_term(
        'Case Vile',
        'category',
        array(
        'description' => 'In aceasta categorie se vor importa toate ofertele de tip case  / vile',
        'slug'        => 'casevile'
        )
    );
}
/******************************************************************************************************
CREATE SPATII CATEGORY
/*****************************************************************************************************/
 function spatii_category() {
    wp_insert_term(
        'Spatii',
        'category',
        array(
        'description' => 'In aceasta categorie se vor importa toate ofertele de tip spatii',
        'slug'        => 'spatii'
        )
    );
}
/******************************************************************************************************
CREATE TERENURI CATEGORY
/*****************************************************************************************************/

function create_taxonomy_record($args) {
    $parent_term = 0;
    if (!empty($args[2]) && ($parent_term = term_exists($args[2], $args[1]))) {
       $parent_term = $parent_term['term_id'];
    }

    $term = term_exists($args[0], $args[1]);
    if ($term == 0 || $term == null) {
        wp_insert_term(
            $args[0], // the term
            $args[1], // the taxonomy
            array(
                'parent'      => $parent_term,
                'description' => $args[3],
                'slug'        => $args[4],
            )
        );
    }

    else { echo "<h1>" . $args[0] . " exists in parent " . $args[1] ."</h1>";}
}

function  createstatus(){


    $parent_term = term_exists( "De vanzare" , 'property-status'  ); // array is returned if taxonomy is given

        if ( $parent_term !== 0 && $parent_term !== null ) {


        }

        else {

             wp_insert_term(

                "De vanzare", // the term
                'property-status', // the taxonomy

                array(

                    'description'=> 'Toate anunturile din categoria vanzari',
                    'slug' => "de_vanzare",

                )

            );


        }




        $parent_term2 = term_exists( "De inchiriat" , 'property-status'  ); // array is returned if taxonomy is given

        if ( $parent_term2 !== 0 && $parent_term !== null ) {


        }

        else{

            wp_insert_term(

                "De inchiriat", // the term
                'property-status', // the taxonomy

                array(

                    'description'=> 'Toate anunturile din categoria de inchiriat',
                    'slug' => "de_inchiriat",

                )

            );


        }



}

// add_action("init","createstatus");


function createTaxonomies_for_records(){


    $tax = array();
    $taxonomii= array();
    set_time_limit(0);


    if (isset($_GET['import-taxonomyes'])) {


    	    $xml = simplexml_load_file(MY_PLUGIN_PATH .'/xml/oferte.xml') or die("Error: Cannot create object");

        if (ob_get_level() == 0) ob_start();


        foreach ( $xml->oferte->oferta as $item ) {

            $statusdevanzare = (string)$item->devanzare;
            $statusdeinchiriat = (string)$item->deinchiriat;


            $parent_term = term_exists( (string)base64_decode($item->localitate) , 'property-city'  ); // array is returned if taxonomy is given

                if ( $parent_term !== 0 && $parent_term !== null ) {

                    continue;
                }


                $tax['oras'] = wp_insert_term(
                   (string)base64_decode($item->localitate), // the term
                  'property-city', // the taxonomy
                  array(
                    'description'=> 'Toate anunturile din orasul'. (string)base64_decode($item->localitate),
                    'slug' => str_replace(' ', '_', (string)base64_decode($item->localitate)),

                  )
                );

                $tip = term_exists( $item->attributes()->tip , 'property-type'); // array is returned if taxonomy is given

                if ( $tip !== 0 && $tip !== null ) {

                    continue;
                }



                $tax['tipanunt'] = wp_insert_term(
                    $item->attributes()->tip, // the term
                  'property-type', // the taxonomy
                  array(
                    'description'=> 'Toate anunturile din categoria'.$item->attributes()->tip,
                    'slug' => str_replace(' ', '_', $item->attributes()->tip),

                  )
            );

			ob_flush();
			flush();

        }

        return $taxonomii[]= $tax;


    }



}

// add_action("init","createTaxonomies_for_records");

function __update_post_meta( $post_id, $field_name, $value = '' )

{
    if ( empty( $value ) OR ! $value )
    {
        delete_post_meta( $post_id, $field_name );
    }
    elseif ( ! get_post_meta( $post_id, $field_name ) )
    {
        add_post_meta( $post_id, $field_name, $value );
    }
    else
    {
        update_post_meta( $post_id, $field_name, $value );
    }
}

/* Import media from url
 *
 * @param string $file_url URL of the existing file from the original site
 * @param int $post_id The post ID of the post to which the imported media is to be attached
 *
 * @return boolean True on success, false on failure
 */

function fetch_media($file_url, $post_id) {
    error_reporting(0);
    require_once(ABSPATH . 'wp-load.php');
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    global $wpdb;

    if(!$post_id) {
        return false;
    }

    //directory to import to
    $artDir = 'wp-content/uploads/importedmedia/';

    //if the directory doesn't exist, create it
    if(!file_exists(ABSPATH.$artDir)) {
        mkdir(ABSPATH.$artDir);
    }

    //rename the file... alternatively, you could explode on "/" and keep the original file name
    $ext = array_pop(explode(".", $file_url));
    $new_filename = 'blogmedia-'.$post_id.".".$ext; //if your post has multiple files, you may need to add a random number to the file name to prevent overwrites

    if (@fclose(@fopen($file_url, "r"))) { //make sure the file actually exists
        copy($file_url, ABSPATH.$artDir.$new_filename);

        $siteurl = get_option('siteurl');
        $file_info = getimagesize(ABSPATH.$artDir.$new_filename);

        //create an array of attachment data to insert into wp_posts table
        $artdata = array();
        $artdata = array(
            'post_author' => 1,
            'post_date' => current_time('mysql'),
            'post_date_gmt' => current_time('mysql'),
            'post_title' => $new_filename,
            'post_status' => 'inherit',
            'comment_status' => 'closed',
            'ping_status' => 'closed',
            'post_name' => sanitize_title_with_dashes(str_replace("_", "-", $new_filename)),                                            'post_modified' => current_time('mysql'),
            'post_modified_gmt' => current_time('mysql'),
            'post_parent' => $post_id,
            'post_type' => 'attachment',
            'guid' => $siteurl.'/'.$artDir.$new_filename,
            'post_mime_type' => $file_info['mime'],
            'post_excerpt' => '',
            'post_content' => ''
        );

        $uploads = wp_upload_dir();
        $save_path = $uploads['basedir'].'/importedmedia/'.$new_filename;

        //insert the database record
        $attach_id = wp_insert_attachment( $artdata, $save_path, $post_id );

        //generate metadata and thumbnails
        if ($attach_data = wp_generate_attachment_metadata( $attach_id, $save_path)) {
            wp_update_attachment_metadata($attach_id, $attach_data);
        }

        //optional make it the featured image of the post it's attached to
        $rows_affected = $wpdb->insert($wpdb->prefix.'postmeta', array('post_id' => $post_id, 'meta_key' => '_thumbnail_id', 'meta_value' => $attach_id));
    }
    else {
        return false;
    }

    return true;
}

/******************************************************************************************************
IF POST EXISTS
/*****************************************************************************************************/

function wp_exist_post_by_title( $title ) {
    global $wpdb;
    $return = $wpdb->get_row( "SELECT ID FROM wp_posts WHERE post_title = '" . $title . "' && post_status = 'publish' && post_type = 'property' ", 'ARRAY_N' );
    if( empty( $return ) ) {
        return false;
    } else {
        return true;
    }
}

/******************************************************************************************************
POPULATE XML FILES
/*****************************************************************************************************/
function populate_xml_files_importer(){

        set_time_limit(0);
        $anunt = array();
        $judet = array( );
        $apartamente_all = array();
        $imagini = array();



        $xml = simplexml_load_file(MY_PLUGIN_PATH .'/xml/oferte.xml') or die("Error: Cannot create object");
       if (ob_get_level() == 0) ob_start();


        foreach ( $xml->oferte->oferta as $item ) {


                $prefix = 'REAL_HOMES_';


            if( wp_exist_post_by_title( (string) base64_decode($item->titlu->lang) ) )

            {
            global $wpdb;
           $id = $wpdb->get_row("SELECT ID FROM wp_posts WHERE post_title = '" . (string) base64_decode($item->titlu->lang) . "' && post_status = 'publish' && post_type = 'property' ", 'ARRAY_N');

                $content = null;

               if (isset($item->descriere->lang)) {
                    $content = (string) base64_decode($item->descriere->lang);
                }


                // if (isset ($item->altedetalii->lang)){
                //     $content .= (string) base64_decode($item->altedetalii->lang);
                // }

                $content .= "<ul>";
                if (isset ($item->disponibilitateproprietate->lang)) {
                      $content .= "<li><strong> Disponibilitate: </strong>".(string) base64_decode($item->disponibilitateproprietate->lang)."</li>";
                }

                if (isset($item->tiplocuinta->lang)) {
                    $content .= "<li><strong> Tip locuinta: </strong>".(string) base64_decode($item->tiplocuinta->lang)."</li>";
                }

                if (isset($item->tipcompartimentare->lang)) {
                    $content .= "<li><strong> Tip compartimentare: </strong>".(string) base64_decode($item->tipcompartimentare->lang)."</li>";
                }
                if (isset($item->etaj->lang)) {
                $content .= "<li><strong> Etaj: </strong>".(string) base64_decode($item->etaj->lang)."</li>";
                    # code...
                }

                if (isset($item->confort->lang)) {
                $content .= "<li><strong> Confort: </strong>".(string) base64_decode($item->confort->lang)."</li>";
                    # code...
                }
                if (isset($item->stadiuconstructie->lang)) {
                $content .= "<li><strong> Stadiu constructie: </strong>".(string) base64_decode($item->stadiuconstructie->lang)."</li>";
                    # code...
                }
                if (isset($item->tipimobil->lang)) {
                $content .= "<li><strong> Tip imobil: </strong>".(string) base64_decode($item->tipimobil->lang)."</li>";
                    # code...
                }
                if (isset($item->structurarezistenta->lang)) {
                $content .= "<li><strong> Structura rezistenta: </strong>".(string) base64_decode($item->structurarezistenta->lang)."</li>";
                    # code...
                }
                if (isset($item->anconstructie->lang)) {
                $content .= "<li><strong> An constructie: </strong>".(string) base64_decode($item->anconstructie->lang)."</li>";

                }
                if (isset($item->altedetaliizona->lang)) {
                $content .= "<li><strong> Alte detalii zona: </strong>".(string) base64_decode($item->altedetaliizona->lang)."</li>";
                    # code...
                }
                if (isset($item->utilitati->lang)) {
                $content .= "<li><strong> Utilitati: </strong>".(string) base64_decode($item->utilitati->lang)."</li>";

                }
                if (isset($item->finisaje->lang)) {
                $content .= "<li><strong> Finisaje: </strong>".(string) base64_decode($item->finisaje->lang)."</li>";

                }

                if (isset($item->dotari->lang)){
                $content .= "<li><strong> Dotari: </strong>".(string) base64_decode($item->dotari->lang)."</li>";

                }


                $content .= "</ul>";


                $anunt['id'] = wp_update_post(array (
                    'ID' => $id,
                    'post_title'    => (string) base64_decode($item->titlu->lang).'-'.(string) base64_decode($item->idstr),
                    'post_type'     => 'property',
                    'post_content' =>     $content,
                    'post_status'   => 'publish',
                    'post_author' => 1,
                ));

               $anunt['1']= update_post_meta( $anunt['id'], "{$prefix}property_price", (string)$item->pretvanzare ." - ". (string)base64_decode($item->monedavanzare) );
                $anunt['2']= update_post_meta( $anunt['id'], "{$prefix}property_size",  (string)$item->suprafatautila );
                $anunt['3']= update_post_meta( $anunt['id'], "{$prefix}property_bedrooms", (string)$item->nrcamere );
                $anunt['4']= update_post_meta( $anunt['id'], "{$prefix}property_bathrooms", (string)$item->nrbai );
                $anunt['5']= update_post_meta( $anunt['id'], "{$prefix}property_garage", (string)$item->nrgaraje );
                $anunt['6']= update_post_meta( $anunt['id'], "{$prefix}property_address", (string)$item->zona );
                $anunt['7']= update_post_meta( $anunt['id'], "{$prefix}property_id", (string) base64_decode($item->idstr) );


            }

            else

            {
                          $content = null;

               if (isset($item->descriere->lang)) {
                    $content = (string) base64_decode($item->descriere->lang);
                }


                // if (isset ($item->altedetalii->lang)){
                //     $content .= (string) base64_decode($item->altedetalii->lang);
                // }

                $content .= "<ul>";
                if (isset ($item->disponibilitateproprietate->lang)) {
                      $content .= "<li><strong> Disponibilitate: </strong>".(string) base64_decode($item->disponibilitateproprietate->lang)."</li>";
                }

                if (isset($item->tiplocuinta->lang)) {
                    $content .= "<li><strong> Tip locuinta: </strong>".(string) base64_decode($item->tiplocuinta->lang)."</li>";
                }

                if (isset($item->tipcompartimentare->lang)) {
                    $content .= "<li><strong> Tip compartimentare: </strong>".(string) base64_decode($item->tipcompartimentare->lang)."</li>";
                }
                if (isset($item->etaj->lang)) {
                $content .= "<li><strong> Etaj: </strong>".(string) base64_decode($item->etaj->lang)."</li>";
                    # code...
                }

                if (isset($item->confort->lang)) {
                $content .= "<li><strong> Confort: </strong>".(string) base64_decode($item->confort->lang)."</li>";
                    # code...
                }
                if (isset($item->stadiuconstructie->lang)) {
                $content .= "<li><strong> Stadiu constructie: </strong>".(string) base64_decode($item->stadiuconstructie->lang)."</li>";
                    # code...
                }
                if (isset($item->tipimobil->lang)) {
                $content .= "<li><strong> Tip imobil: </strong>".(string) base64_decode($item->tipimobil->lang)."</li>";
                    # code...
                }
                if (isset($item->structurarezistenta->lang)) {
                $content .= "<li><strong> Structura rezistenta: </strong>".(string) base64_decode($item->structurarezistenta->lang)."</li>";
                    # code...
                }
                if (isset($item->anconstructie->lang)) {
                $content .= "<li><strong> An constructie: </strong>".(string) base64_decode($item->anconstructie->lang)."</li>";

                }
                if (isset($item->altedetaliizona->lang)) {
                $content .= "<li><strong> Alte detalii zona: </strong>".(string) base64_decode($item->altedetaliizona->lang)."</li>";
                    # code...
                }
                if (isset($item->utilitati->lang)) {
                $content .= "<li><strong> Utilitati: </strong>".(string) base64_decode($item->utilitati->lang)."</li>";

                }
                if (isset($item->finisaje->lang)) {
                $content .= "<li><strong> Finisaje: </strong>".(string) base64_decode($item->finisaje->lang)."</li>";

                }

                if (isset($item->dotari->lang)) {
                $content .= "<li><strong> Dotari: </strong>".(string) base64_decode($item->dotari->lang)."</li>";

                }


                $content .= "</ul>";

                $anunt['id'] = wp_insert_post(array (
                    'post_title'    => (string) base64_decode($item->titlu->lang).'-'.(string) base64_decode($item->idstr),
                    'post_type'     => 'property',
                    'post_content' =>    $content,
                    'post_status'   => 'publish',
                    'post_author' => 1,

                     'meta_input' => array(

                        "{$prefix}property_price" =>  (string)$item->pretvanzare ." - ". (string)base64_decode($item->monedavanzare),
                        "{$prefix}property_size"=> (string)$item->suprafatautila,
                        "{$prefix}property_bedrooms"=> (string)$item->nrcamere,
                        "{$prefix}property_bathrooms"=> (string)$item->nrbai,
                        "{$prefix}property_garage"=>(string)$item->nrgaraje,
                        // "{$prefix}property_garage"=>(string)$item->nrbucatarii,
                        "{$prefix}property_address"=> (string)$item->zona,
                        "{$prefix}property_id"=> (string) base64_decode($item->idstr),

                        ),

                ));


            // post does not exist
            }




                $anunt['type'] = wp_set_object_terms( $anunt['id'],  $item->attributes()->tip , "property-type" );
                $anunt['city'] = wp_set_object_terms( $anunt['id'],  (string)base64_decode($item->localitate) , "property-city" );


                $statusdevanzare = (string)$item->devanzare;
                $statusdeinchiriat = (string)$item->deinchiriat;

                if ( (string)$item->deinchiriat == '1') {

                    $anunt['deinchiriat'] = wp_set_object_terms( $anunt['id'],  "De inchiriat" , "property-status" );

                }

                if ( (string)$item->devanzare == '1') {

                    $anunt['devanzare'] = wp_set_object_terms( $anunt['id'],  "De vanzare" , "property-status" );

                }

                 $galleryimages= array();

                 foreach ( $item->imagini->imagine as $img ) {

                    $srcimagine = str_replace('_IMG_LATIMExIMG_INALTIME', '_1000x800',  (string) $img->src , $count);

                    $galleryimages[] = str_replace('_IMG_LATIMExIMG_INALTIME', '_100x80',  (string) $img->src , $count);


                }


                fetch_media($srcimagine, $anunt['id']);

                $anunt['gallery'] = __update_post_meta(   $anunt['id'] , "{$prefix}property_images_custom" ,   $galleryimages, true );


                $anunt['location'] = __update_post_meta(   $anunt['id'] , "{$prefix}gallery_slider_type" , "thumb-on-bottom", true );





            $x = simplexml_load_file(MY_PLUGIN_PATH .'/xml/agenti.xml') or die("Error: Cannot create object");

            foreach ($x->agent as $moderator) {

                if ((string)$item->agent == (string)$moderator->agentid ) {


                     $anunt['activate'] = __update_post_meta(   $anunt['id'] ,  "{$prefix}detaliiagent" , "yes" , true );
                     $anunt['emailagent'] = __update_post_meta(   $anunt['id'] ,  "{$prefix}agent_email2" , (string) base64_decode($moderator-> email) , true );
                     $anunt['mobile_number'] = __update_post_meta(   $anunt['id'] ,   "{$prefix}mobile_number2" , (string) base64_decode($moderator-> mobil) , true );
                     $anunt['office_number'] = __update_post_meta(   $anunt['id'] ,   "{$prefix}office_number2"  , (string) base64_decode($moderator-> telefon) , true );
                     $anunt['office_nume'] = __update_post_meta(   $anunt['id'] ,   "{$prefix}agent_name2"  , (string) base64_decode($moderator->nume) , true );
                     $anunt['agent_functie'] = __update_post_meta(   $anunt['id'] ,   "{$prefix}agent_functie"    , (string) base64_decode($moderator-> functie) , true );
                     $anunt['agent_img'] = __update_post_meta(   $anunt['id'] ,    "{$prefix}agent_img"    , "<img style='width:100px; height:auto;' src='data:image/jpeg;base64,".(string)base64_decode($moderator->poza)."'>" , true );



                }

				ob_flush();
				flush();
            }

            }

            $apartamente_all[] = $anunt;


           $apartamente_all;

           die();



}

function wpmete_on_activation_wc(){

    $wpmetetxt = "
    # WP Maximum Execution Time Exceeded
    <IfModule mod_php5.c>
        php_value max_execution_time 300
    </IfModule>";

    $htaccess = get_home_path().'.htaccess';
    $contents = @file_get_contents($htaccess);
    if(!strpos($htaccess,$wpmetetxt))
    file_put_contents($htaccess,$contents.$wpmetetxt);
}



/* On deactivation delete code (dc) from htaccess file */

function wpmete_on_deactivation_dc(){

    $wpmetetxt = "
    # WP Maximum Execution Time Exceeded
    <IfModule mod_php5.c>
        php_value max_execution_time 300
    </IfModule>";

    $htaccess = get_home_path().'.htaccess';
    $contents = @file_get_contents($htaccess);
    file_put_contents($htaccess,str_replace($wpmetetxt,'',$contents));

}


register_activation_hook(   __FILE__, 'wpmete_on_activation_wc' );

register_deactivation_hook( __FILE__, 'wpmete_on_deactivation_dc' );

