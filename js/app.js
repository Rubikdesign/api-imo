        jQuery(document).ready(function(){
            jQuery('#prog').progressbar({ value: 0 });
            console.log("REady Api");

            jQuery(document).on('click', '#import-anunturi', function (e) {

            e.preventDefault();
             var beforeload = new Date();// initialize before sending ajax call not globally
             var loading = jQuery(".progress-bar");


                    var data = {
                        action: 'populate_xml_files_importer',
                        // module_name: add_module_name,
                        // module_caption: add_module_caption,
                        // module_slug: add_module_slug,
                        // // tinymce_activation_class: tinyrand,
                        // postid: post_id
                    };

                    jQuery.ajax({

                        url: ajaxurl,  /* Admin ajax url from localized script */
                        type: 'POST',  /* Important */
                        data: data,
                        start_time: new Date().getTime(),  /* Data object including 'action' and all post variables */

                        xhr: function () {
                            var xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                    console.log(percentComplete);
                                    jQuery('.progress-script').css({
                                        width: percentComplete * 100 + '%'
                                    });
                                    if (percentComplete === 1) {
                                        // jQuery('.progress-script').addClass('hide');
                                    }
                                }
                            }, false);
                            xhr.addEventListener("progress-script", function (evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                    console.log(percentComplete);
                                    jQuery('.progress-script').css({
                                        width: percentComplete * 100 + '%'
                                    });
                                }
                            }, false);
                            return xhr;
                        },

                        beforeSend: function() {

                            jQuery("#process").fadeIn(0);
                            jQuery(".app-setting-wrapp").slideUp(0);
                             jQuery("#succes").fadeOut(0);
                        },



                        success : function(response) {

                            // console.log(response);
                            jQuery("#process").fadeOut(0);
                            // jQuery(".app-setting-wrapp").fadeIn(0);
                            jQuery("#succes").fadeIn(0);
                            jQuery("#succes").html('This request took '+(new Date().getTime() - this.start_time)+' ms');
                            // jQuery jQuery("#succes").fadeIn(0);('ul#zet-components').append( response );
                            // resize_elemets();
                            // render_esitor();
                        },

                        complete: function(response, status, xhr) {

                            jQuery("#succes").fadeOut(2950);
                            setTimeout(function(){ jQuery(".app-setting-wrapp").slideDown(1000); }, 3000);

                        }
                    });

            });




            jQuery(document).on('click', '#delete-anunturi', function (e) {

            e.preventDefault();



                    var data = {
                        action: 'delete_anunturi',
                        // module_name: add_module_name,
                        // module_caption: add_module_caption,
                        // module_slug: add_module_slug,
                        // // tinymce_activation_class: tinyrand,
                        // postid: post_id
                    };

                    jQuery.ajax({

                        url: ajaxurldel,  /* Admin ajax url from localized script */
                        type: 'POST',  /* Important */
                        data: data,  /* Data object including 'action' and all post variables */
                        beforeSend: function() {

                            jQuery("#process").fadeIn(0);
                            jQuery(".app-setting-wrapp").slideUp(0);
                             jQuery("#succes").fadeOut(0);
                        },
                        success : function(response) {

                          console.log(response);
                            jQuery("#process").fadeOut(0);
                            // jQuery(".app-setting-wrapp").fadeIn(0);
                            jQuery("#succes").fadeIn(0);
                            // jQuery jQuery("#succes").fadeIn(0);('ul#zet-components').append( response );
                            // resize_elemets();
                            // render_esitor();
                        },
                        complete: function() {

                            jQuery("#succes").fadeOut(2950);

                            setTimeout(function(){ jQuery(".app-setting-wrapp").slideDown(1000); }, 3000);

                        }
                    });

            });




        });

